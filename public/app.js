// import { initializeApp } from "firebase/app";
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.4.0/firebase-app.js";
import { getFirestore, doc, getDoc, getDocs, collection } from "https://www.gstatic.com/firebasejs/9.4.0/firebase-firestore.js";
const firebaseConfig = {
    apiKey: "AIzaSyBJ7qSiwxZ0KY2BFwYr64-HltN-ZCwubZc",
    authDomain: "mikhunaapp-8b36f.firebaseapp.com",
    projectId: "mikhunaapp-8b36f",
    storageBucket: "mikhunaapp-8b36f.appspot.com",
    messagingSenderId: "24807176897",
    appId: "1:24807176897:web:c7caff1b461732a5653240",
    measurementId: "G-B1M6RL50V5"
  };
const asc = arr => arr.sort((a, b) => a - b);
const quantile = (arr, q) => {
    const sorted = asc(arr);
    const pos = (sorted.length - 1) * q;
    const base = Math.floor(pos);
    const rest = pos - base;
    if (sorted[base + 1] !== undefined) {
        return sorted[base] + rest * (sorted[base + 1] - sorted[base]);
    } else {
        return sorted[base];
    }
};
const q25 = arr => quantile(arr, .25);
const q50 = arr => quantile(arr, .50);
const q75 = arr => quantile(arr, .75);



document.addEventListener("DOMContentLoaded", event => {
    const app = initializeApp(firebaseConfig);
    const db = getFirestore(app);

    const t5n2Col = collection(db,'t5n2');
    const t3n4Col = collection(db,'t3n4');
    const recipesCol = collection(db,'recipes');

    getDocs(t3n4Col).then((querySnapshot)=>{
        let absoluteMax=0;
        let absoluteMin=999999999999;
        let info = querySnapshot.docs.map((doc)=>{
            let data=doc.data()
            let times = data['times'].map((act)=>act/1000);
            let max = Math.max(...times);
            let min = Math.min(...times)
            absoluteMin=min<absoluteMin?min:absoluteMin;
            absoluteMax=max>absoluteMax?max:absoluteMax;
            let py = [
                min,
                q25(times),
                q50(times),
                q75(times),
                max
            ];
            return {
                x:doc['id'],
                y:py
            }});
        absoluteMax=absoluteMax+Math.max(...[1,absoluteMin,absoluteMax/10]);
        var options = {
            series: [
            {
              type: 'boxPlot',
              data: info
            }
          ],
            chart: {
            type: 'boxPlot',
            height: 400
          },
          title: {
            text: 'Time distribution for accessing shopping list per skill demographic',
            align: 'left'
          },
          yaxis:{
            min:-2,
            max:absoluteMax,
            title:{
                text: 'T(s)'
            }
          },
          xaxis:{
            title:{
                text:'Technical skill',
                offsetY:90
            }
          },
          plotOptions: {
            boxPlot: {
              colors: {
                upper: '#e38d72',
                lower: '#f4ebd1'
              }
            }
          }
          };
  
          var chart = new ApexCharts(document.querySelector("#boxGraphT3N4"), options);
          chart.render();
    });

    getDocs(t5n2Col).then((querySnapshot)=>{
        let absoluteMax=0;
        let absoluteMin=999999999999;
        let info = querySnapshot.docs.map((doc)=>{
            let data=doc.data()
            let times = data['times'];
            let max = Math.max(...times);
            let min = Math.min(...times)
            absoluteMin=min<absoluteMin?min:absoluteMin;
            absoluteMax=max>absoluteMax?max:absoluteMax;
            let py = [
                min,
                q25(times),
                q50(times),
                q75(times),
                max
            ];
            return {
                x:data['phone'],
                y:py
            }});
            absoluteMax=absoluteMax+Math.max(...[1,absoluteMin,absoluteMax/10]);
        var options = {
            series: [
            {
              type: 'boxPlot',
              data: info
            }
          ],
            chart: {
            type: 'boxPlot',
            height: 400
          },
          title: {
            text: 'Time distribution for calculating number of ingredients per phone',
            align: 'left'
          },
          yaxis:{
            min:-2,
            max:absoluteMax,
            title:{
                text: 'T(ms)'
            }
          },
          xaxis:{
            title:{
                text:'Phone',
                offsetY:90
            }
          },
          plotOptions: {
            boxPlot: {
              colors: {
                upper: '#e38d72',
                lower: '#f4ebd1'
              }
            }
          }
          };
  
          var chart = new ApexCharts(document.querySelector("#boxGraph"), options);
          chart.render();
    });


    getDocs(recipesCol).then((querySnapshot)=>{
        let recipeUsers = [];
        querySnapshot.docs.forEach((doc)=>{
            if (doc.data().userId !== undefined) {
                recipeUsers.push(doc.data().userId);
            }
        });
        let userSetSize = new Set(recipeUsers).size;

        var options = {
            chart: {
                height: 280,
                type: "radialBar",
            },
    
            series: [userSetSize*100/5],
            colors: ["#20E647"],
            plotOptions: {
                radialBar: {
                    hollow: {
                        margin: 0,
                        size: "70%",
                        background: "#e38d72"
                    },
                    track: {
                        dropShadow: {
                            enabled: true,
                            top: 2,
                            left: 0,
                            blur: 4,
                            opacity: 0.15
                        }
                    },
                    dataLabels: {
                        name: {
                            offsetY: -10,
                            color: "#362c28",
                            fontSize: "13px"
                        },
                        value: {
                            color: "#362c28",
                            fontSize: "30px",
                            show: true
                        }
                    }
                }
            },
            fill: {
                type: "gradient",
                gradient: {
                    shade: "dark",
                    type: "horizontal",
                    shadeIntensity:0.25,
                    gradientToColors: ["#4cae63"],
                    stops: [0, 100]
                }
            },
            stroke: {
                lineCap: "round"
            },
            labels: ["Users adding recipes"]
        };
        var chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();
    });

});

